(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

function EventEmitter() {
  this._events = this._events || {};
  this._maxListeners = this._maxListeners || undefined;
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
EventEmitter.defaultMaxListeners = 10;

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!isNumber(n) || n < 0 || isNaN(n))
    throw TypeError('n must be a positive number');
  this._maxListeners = n;
  return this;
};

EventEmitter.prototype.emit = function(type) {
  var er, handler, len, args, i, listeners;

  if (!this._events)
    this._events = {};

  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events.error ||
        (isObject(this._events.error) && !this._events.error.length)) {
      er = arguments[1];
      if (er instanceof Error) {
        throw er; // Unhandled 'error' event
      } else {
        // At least give some kind of context to the user
        var err = new Error('Uncaught, unspecified "error" event. (' + er + ')');
        err.context = er;
        throw err;
      }
    }
  }

  handler = this._events[type];

  if (isUndefined(handler))
    return false;

  if (isFunction(handler)) {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        args = Array.prototype.slice.call(arguments, 1);
        handler.apply(this, args);
    }
  } else if (isObject(handler)) {
    args = Array.prototype.slice.call(arguments, 1);
    listeners = handler.slice();
    len = listeners.length;
    for (i = 0; i < len; i++)
      listeners[i].apply(this, args);
  }

  return true;
};

EventEmitter.prototype.addListener = function(type, listener) {
  var m;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events)
    this._events = {};

  // To avoid recursion in the case that type === "newListener"! Before
  // adding it to the listeners, first emit "newListener".
  if (this._events.newListener)
    this.emit('newListener', type,
              isFunction(listener.listener) ?
              listener.listener : listener);

  if (!this._events[type])
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  else if (isObject(this._events[type]))
    // If we've already got an array, just append.
    this._events[type].push(listener);
  else
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];

  // Check for listener leak
  if (isObject(this._events[type]) && !this._events[type].warned) {
    if (!isUndefined(this._maxListeners)) {
      m = this._maxListeners;
    } else {
      m = EventEmitter.defaultMaxListeners;
    }

    if (m && m > 0 && this._events[type].length > m) {
      this._events[type].warned = true;
      console.error('(node) warning: possible EventEmitter memory ' +
                    'leak detected. %d listeners added. ' +
                    'Use emitter.setMaxListeners() to increase limit.',
                    this._events[type].length);
      if (typeof console.trace === 'function') {
        // not supported in IE 10
        console.trace();
      }
    }
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  var fired = false;

  function g() {
    this.removeListener(type, g);

    if (!fired) {
      fired = true;
      listener.apply(this, arguments);
    }
  }

  g.listener = listener;
  this.on(type, g);

  return this;
};

// emits a 'removeListener' event iff the listener was removed
EventEmitter.prototype.removeListener = function(type, listener) {
  var list, position, length, i;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events || !this._events[type])
    return this;

  list = this._events[type];
  length = list.length;
  position = -1;

  if (list === listener ||
      (isFunction(list.listener) && list.listener === listener)) {
    delete this._events[type];
    if (this._events.removeListener)
      this.emit('removeListener', type, listener);

  } else if (isObject(list)) {
    for (i = length; i-- > 0;) {
      if (list[i] === listener ||
          (list[i].listener && list[i].listener === listener)) {
        position = i;
        break;
      }
    }

    if (position < 0)
      return this;

    if (list.length === 1) {
      list.length = 0;
      delete this._events[type];
    } else {
      list.splice(position, 1);
    }

    if (this._events.removeListener)
      this.emit('removeListener', type, listener);
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  var key, listeners;

  if (!this._events)
    return this;

  // not listening for removeListener, no need to emit
  if (!this._events.removeListener) {
    if (arguments.length === 0)
      this._events = {};
    else if (this._events[type])
      delete this._events[type];
    return this;
  }

  // emit removeListener for all listeners on all events
  if (arguments.length === 0) {
    for (key in this._events) {
      if (key === 'removeListener') continue;
      this.removeAllListeners(key);
    }
    this.removeAllListeners('removeListener');
    this._events = {};
    return this;
  }

  listeners = this._events[type];

  if (isFunction(listeners)) {
    this.removeListener(type, listeners);
  } else if (listeners) {
    // LIFO order
    while (listeners.length)
      this.removeListener(type, listeners[listeners.length - 1]);
  }
  delete this._events[type];

  return this;
};

EventEmitter.prototype.listeners = function(type) {
  var ret;
  if (!this._events || !this._events[type])
    ret = [];
  else if (isFunction(this._events[type]))
    ret = [this._events[type]];
  else
    ret = this._events[type].slice();
  return ret;
};

EventEmitter.prototype.listenerCount = function(type) {
  if (this._events) {
    var evlistener = this._events[type];

    if (isFunction(evlistener))
      return 1;
    else if (evlistener)
      return evlistener.length;
  }
  return 0;
};

EventEmitter.listenerCount = function(emitter, type) {
  return emitter.listenerCount(type);
};

function isFunction(arg) {
  return typeof arg === 'function';
}

function isNumber(arg) {
  return typeof arg === 'number';
}

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}

function isUndefined(arg) {
  return arg === void 0;
}

},{}],2:[function(require,module,exports){
var EventEmitter = require('events');
var Rectangle = require('./actors/Rectangle');
var Circle = require('./actors/Circle');
var AiPaddle = require('./actors/GameAIPaddle');

var Game = require('./Game.js');

var App = function () {
    this.eventEmitter = new EventEmitter();
    
    this.canvas = document.getElementById('gameCanvas');
    this.canvasContext = this.canvas.getContext('2d'); 
   
    this.ball = new Circle({ x: 400, y: 400 }, 20, 0, Math.PI * 2, false, this.canvasContext, { x: this.canvas.width, y: this.canvas.height });
    this.leftPaddle = new Rectangle({ x: 0, y: 50 }, 20, 150, "red", this.canvasContext);
    this.rightPaddle = new AiPaddle({ x: 780, y: 300 }, 20, 150, "red", this.canvasContext);
    
    var actors = {
        ball: this.ball,
        leftPaddle: this.leftPaddle,
        rightPaddle: this.rightPaddle  
    };
    
    this.onMousemoove();
    var game = new Game(actors, this.canvas, this.canvasContext, {framesPerSecond: 120, loopFrequency: 1000 / 120});
    
    game.start(); 
};

App.prototype.calculateMousePos = function (event) {
    var rect = this.canvas.getBoundingClientRect();
    var root = document.documentElement;
 
    return {
        x: event.clientX - rect.left - root.scrollLeft,
        y: event.clientY - rect.top - root.scrollTop,
    };
};

// App.prototype.moveAll = function () {
//     this.ball.move();
// };

App.prototype.onMissed = function () {
    eventEmitter.on('missed', function () {
        //TODO: Game Over
    });
};

App.prototype.onMousemoove = function (params) {
    this.canvas.addEventListener('mousemove', function (event) {
        var mousePos = this.calculateMousePos(event);
        this.leftPaddle.move(mousePos);
    }.bind(this));
}; 

module.exports = App;

var app = new App();

},{"./Game.js":3,"./actors/Circle":4,"./actors/GameAIPaddle":5,"./actors/Rectangle":6,"events":1}],3:[function(require,module,exports){
var Physics = require('./helpers/Physics');
var Renderer = require('./helpers/Renderer');
var Rectangle = require('./actors/Rectangle');
var Circle = require('./actors/Circle');
var AiPaddle = require('./actors/GameAIPaddle');

var Game = function (actors, canvas, canvasContext, options) {
    if (!actors) {
        throw new this.noActorsExeption('Actors must not be null or undefined!');
    }

    this.actors = actors;
    this.physics = new Physics(actors, canvas);

    this.renderer = new Renderer(actors, canvas, canvasContext);
    this.framesPerSecond = options.framesPerSecond || 120;
    this.loopFrequency = options.loopFrequency || 1000 / 120;
};

Game.prototype.start = function () {
    this.loop();
};

Game.prototype.noActorsExeption = function (msg) {
    this.message = msg;
    this.name = 'NoActorsExeption';
};

Game.prototype.loopLogic = function () {
    this.renderer.drawAll();
    this.actors.ball.move();
    this.physics.followBallPos();

    this.actors.rightPaddle.followBall(this.actors.ball.position);
};

Game.prototype.loop = function () {
    this.gameLoop = setInterval(this.loopLogic.bind(this), this.loopFrequency);
};

module.exports = Game;
},{"./actors/Circle":4,"./actors/GameAIPaddle":5,"./actors/Rectangle":6,"./helpers/Physics":7,"./helpers/Renderer":8}],4:[function(require,module,exports){
var EventEmitter = require('events');
var eventEmitter = new EventEmitter();

var Circle = function (position, radius, startAngle, endAngle, anticlockwise, canvasContext, boundaries) {
    this.position = position;
    this.radius = radius;
    this.startAngle = startAngle;
    this.endAngle = endAngle;
    this.anticlockwise = anticlockwise;
    this.canvasContext = canvasContext;
    this.boundaries = boundaries;

    this.speedX = 3.5;
    this.speedY = 2;
};

Circle.prototype.reset = function (position) {
    this.position = position || this.position;
    this.canvasContext.fillStyle = "red";
    this.canvasContext.beginPath();
    this.canvasContext.arc(this.position.x, position.y, this.radius, this.startAngle, this.endAngle, this.anticlockwise); // Outer circle
    this.canvasContext.fill();
};

Circle.prototype.changeXSpeed = function () {
    this.speedX = -1 * this.speedX;
    this.move();
};

Circle.prototype.move = function () {
    this.position.x += this.speedX;
    this.position.y += this.speedY;

    this.reset(this.position);
}


module.exports = Circle;

},{"events":1}],5:[function(require,module,exports){
var Rectangle = require('./Rectangle');

var gameAiPaddle = function (position, width, height, color, canvasContext) {
    Rectangle.call(this, position, width, height, color, canvasContext);
    console.log('AI');
};

gameAiPaddle.prototype = Object.create(Rectangle.prototype);
gameAiPaddle.prototype.constructor = gameAiPaddle;

gameAiPaddle.prototype.followBall = function (ballPos) {
    if(this.position.y > ballPos.y){
        this.position.y -= 6; 
    }else{
        this.position.y += 6;
    }
    // this.draw();
};

module.exports = gameAiPaddle;
},{"./Rectangle":6}],6:[function(require,module,exports){
var Rectangle = function(position, width, height, color, canvasContext){
    this.position = position;
    this.width = width;
    this.height = height;
    this.color = color;
    this.position = position;
    this.canvasContext = canvasContext;
};

Rectangle.prototype.draw = function(position){
    this.position.y = position?position.y:this.position.y;
    this.canvasContext.fillStyle = this.color;
    this.canvasContext.fillRect(this.position.x, this.position.y, this.width, this.height);
};

Rectangle.prototype.move = function (position) {
    position = position || this.position;
    this.draw({x: this.position.x, y: position.y - this.height/2});
};

module.exports = Rectangle;
},{}],7:[function(require,module,exports){
var EventEmitter = require('events');
var eventEmitter = new EventEmitter();

var Physics = function (actors, canvas) {
    this.ball = actors.ball;
    this.leftPaddle = actors.leftPaddle;
    this.rightPaddle = actors.rightPaddle;
    this.canvas = canvas;
};
 
Physics.prototype.followBallPos = function () {
    //if the ball hits the left or the right wall
    if (this.ball.position.x >= this.canvas.width - this.rightPaddle.width || this.ball.position.x <= 0 + this.leftPaddle.width) {
        this.detectCollision();
        // this.ball.changeXSpeed();
        // this.ball.move();
    }
    //if the ball hits the top or the bottom wall
    if (this.ball.position.y >= this.canvas.height || this.ball.position.y <= 0) {
        this.ball.speedY = -1 * this.ball.speedY;
    }
};

Physics.prototype.detectCollision = function () {
    var paddle = (this.ball.position.x <= 0 + this.leftPaddle.width)?this.leftPaddle:this.rightPaddle;
    var paddleTop = paddle.position.y;
    var paddleBottom = paddle.position.y + paddle.height;

    if(this.ball.position.y < paddleTop || this.ball.position.y > paddleBottom){
        //no collision, the player has missed
        eventEmitter.emit('missed');
        this.ball.changeXSpeed();
        this.ball.reset({x: this.canvas.width/2, y: this.canvas.height/2});
    }else if(this.ball.position.y > paddleTop && this.ball.position.y < paddleBottom){
        eventEmitter.emit('hit');
        this.ball.changeXSpeed();
    }
};

module.exports = Physics;
},{"events":1}],8:[function(require,module,exports){
var EventEmitter = require('events');

var Renderer = function (actors, canvas, canvasContext) {
    this.leftPaddle = actors.leftPaddle;
    this.rightPaddle = actors.rightPaddle;
    
    this.canvas = canvas;
    this.canvasContext = canvasContext; 
};
 
Renderer.prototype.drawAll = function () {
    this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.canvasContext.fillStyle = 'black';
    this.canvasContext.fillRect(0, 0, this.canvas.width, this.canvas.height);
    
    this.leftPaddle.draw();
    this.rightPaddle.draw(); 
};
 
Renderer.prototype.getInstanceOfCanvas = function () {
    return {
        canvas: this.canvas,
        canvasContext: this.canvasContext
    };
};

module.exports = Renderer;
},{"events":1}]},{},[7,8,2,3]);
