var EventEmitter = require('events');

var Renderer = function (actors, canvas, canvasContext) {
    this.leftPaddle = actors.leftPaddle;
    this.rightPaddle = actors.rightPaddle;
    
    this.canvas = canvas;
    this.canvasContext = canvasContext; 
};
 
Renderer.prototype.drawAll = function () {
    this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.canvasContext.fillStyle = 'black';
    this.canvasContext.fillRect(0, 0, this.canvas.width, this.canvas.height);
    
    this.leftPaddle.draw();
    this.rightPaddle.draw(); 
};
 
Renderer.prototype.getInstanceOfCanvas = function () {
    return {
        canvas: this.canvas,
        canvasContext: this.canvasContext
    };
};

module.exports = Renderer;