var EventEmitter = require('events');
var eventEmitter = new EventEmitter();

var Physics = function (actors, canvas) {
    this.ball = actors.ball;
    this.leftPaddle = actors.leftPaddle;
    this.rightPaddle = actors.rightPaddle;
    this.canvas = canvas;
};
 
Physics.prototype.followBallPos = function () {
    //if the ball hits the left or the right wall
    if (this.ball.position.x >= this.canvas.width - this.rightPaddle.width || this.ball.position.x <= 0 + this.leftPaddle.width) {
        this.detectCollision();
        // this.ball.changeXSpeed();
        // this.ball.move();
    }
    //if the ball hits the top or the bottom wall
    if (this.ball.position.y >= this.canvas.height || this.ball.position.y <= 0) {
        this.ball.speedY = -1 * this.ball.speedY;
    }
};

Physics.prototype.detectCollision = function () {
    var paddle = (this.ball.position.x <= 0 + this.leftPaddle.width)?this.leftPaddle:this.rightPaddle;
    var paddleTop = paddle.position.y;
    var paddleBottom = paddle.position.y + paddle.height;

    if(this.ball.position.y < paddleTop || this.ball.position.y > paddleBottom){
        //no collision, the player has missed
        eventEmitter.emit('missed');
        this.ball.changeXSpeed();
        this.ball.reset({x: this.canvas.width/2, y: this.canvas.height/2});
    }else if(this.ball.position.y > paddleTop && this.ball.position.y < paddleBottom){
        eventEmitter.emit('hit');
        this.ball.changeXSpeed();
    }
};

module.exports = Physics;