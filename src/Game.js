var Physics = require('./helpers/Physics');
var Renderer = require('./helpers/Renderer');
var Rectangle = require('./actors/Rectangle');
var Circle = require('./actors/Circle');
var AiPaddle = require('./actors/GameAIPaddle');

var Game = function (actors, canvas, canvasContext, options) {
    if (!actors) {
        throw new this.noActorsExeption('Actors must not be null or undefined!');
    }

    this.actors = actors;
    this.physics = new Physics(actors, canvas);

    this.renderer = new Renderer(actors, canvas, canvasContext);
    this.framesPerSecond = options.framesPerSecond || 120;
    this.loopFrequency = options.loopFrequency || 1000 / 120;
};

Game.prototype.start = function () {
    this.loop();
};

Game.prototype.noActorsExeption = function (msg) {
    this.message = msg;
    this.name = 'NoActorsExeption';
};

Game.prototype.loopLogic = function () {
    this.renderer.drawAll();
    this.actors.ball.move();
    this.physics.followBallPos();

    this.actors.rightPaddle.followBall(this.actors.ball.position);
};

Game.prototype.loop = function () {
    this.gameLoop = setInterval(this.loopLogic.bind(this), this.loopFrequency);
};

module.exports = Game;