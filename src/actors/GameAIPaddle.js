var Rectangle = require('./Rectangle');

var gameAiPaddle = function (position, width, height, color, canvasContext) {
    Rectangle.call(this, position, width, height, color, canvasContext);
    console.log('AI');
};

gameAiPaddle.prototype = Object.create(Rectangle.prototype);
gameAiPaddle.prototype.constructor = gameAiPaddle;

gameAiPaddle.prototype.followBall = function (ballPos) {
    if(this.position.y > ballPos.y){
        this.position.y -= 6; 
    }else{
        this.position.y += 6;
    }
    // this.draw();
};

module.exports = gameAiPaddle;