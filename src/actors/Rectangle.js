var Rectangle = function(position, width, height, color, canvasContext){
    this.position = position;
    this.width = width;
    this.height = height;
    this.color = color;
    this.position = position;
    this.canvasContext = canvasContext;
};

Rectangle.prototype.draw = function(position){
    this.position.y = position?position.y:this.position.y;
    this.canvasContext.fillStyle = this.color;
    this.canvasContext.fillRect(this.position.x, this.position.y, this.width, this.height);
};

Rectangle.prototype.move = function (position) {
    position = position || this.position;
    this.draw({x: this.position.x, y: position.y - this.height/2});
};

module.exports = Rectangle;