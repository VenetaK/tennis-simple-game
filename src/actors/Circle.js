var EventEmitter = require('events');
var eventEmitter = new EventEmitter();

var Circle = function (position, radius, startAngle, endAngle, anticlockwise, canvasContext, boundaries) {
    this.position = position;
    this.radius = radius;
    this.startAngle = startAngle;
    this.endAngle = endAngle;
    this.anticlockwise = anticlockwise;
    this.canvasContext = canvasContext;
    this.boundaries = boundaries;

    this.speedX = 3.5;
    this.speedY = 2;
};

Circle.prototype.reset = function (position) {
    this.position = position || this.position;
    this.canvasContext.fillStyle = "red";
    this.canvasContext.beginPath();
    this.canvasContext.arc(this.position.x, position.y, this.radius, this.startAngle, this.endAngle, this.anticlockwise); // Outer circle
    this.canvasContext.fill();
};

Circle.prototype.changeXSpeed = function () {
    this.speedX = -1 * this.speedX;
    this.move();
};

Circle.prototype.move = function () {
    this.position.x += this.speedX;
    this.position.y += this.speedY;

    this.reset(this.position);
}


module.exports = Circle;
