var EventEmitter = require('events');
var Rectangle = require('./actors/Rectangle');
var Circle = require('./actors/Circle');
var AiPaddle = require('./actors/GameAIPaddle');

var Game = require('./Game.js');

var App = function () {
    this.eventEmitter = new EventEmitter();
    
    this.canvas = document.getElementById('gameCanvas');
    this.canvasContext = this.canvas.getContext('2d'); 
   
    this.ball = new Circle({ x: 400, y: 400 }, 20, 0, Math.PI * 2, false, this.canvasContext, { x: this.canvas.width, y: this.canvas.height });
    this.leftPaddle = new Rectangle({ x: 0, y: 50 }, 20, 150, "red", this.canvasContext);
    this.rightPaddle = new AiPaddle({ x: 780, y: 300 }, 20, 150, "red", this.canvasContext);
    
    var actors = {
        ball: this.ball,
        leftPaddle: this.leftPaddle,
        rightPaddle: this.rightPaddle  
    };
    
    this.onMousemoove();
    var game = new Game(actors, this.canvas, this.canvasContext, {framesPerSecond: 120, loopFrequency: 1000 / 120});
    
    game.start(); 
};

App.prototype.calculateMousePos = function (event) {
    var rect = this.canvas.getBoundingClientRect();
    var root = document.documentElement;
 
    return {
        x: event.clientX - rect.left - root.scrollLeft,
        y: event.clientY - rect.top - root.scrollTop,
    };
};

// App.prototype.moveAll = function () {
//     this.ball.move();
// };

App.prototype.onMissed = function () {
    eventEmitter.on('missed', function () {
        //TODO: Game Over
    });
};

App.prototype.onMousemoove = function (params) {
    this.canvas.addEventListener('mousemove', function (event) {
        var mousePos = this.calculateMousePos(event);
        this.leftPaddle.move(mousePos);
    }.bind(this));
}; 

module.exports = App;

var app = new App();
