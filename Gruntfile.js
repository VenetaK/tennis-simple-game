module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            files: ['src/*.js', 'src/actors*.js', 'src/helpers/*.js'],
            tasks: ['browserify'],
            options: {
                spawn: false,
            }
        },
        browserify: {
            dist: {
                files: {
                    'build/build.js': ['src/actors*.js',  'src/helpers/*.js', 'src/*.js']
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.registerTask('default', ['grunt-contrib-watch']);
};